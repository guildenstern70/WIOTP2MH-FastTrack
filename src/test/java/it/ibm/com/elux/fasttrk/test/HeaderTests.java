package it.ibm.com.elux.fasttrk.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.ibm.com.elux.fasttrk.kafka.KafkaHeader;
import it.ibm.com.elux.fasttrk.kafka.KafkaMessage;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 *
 */
public class HeaderTests
{
    private static final Logger logger = Logger.getLogger(HeaderTests.class);

    @Test
    public void headerGeneration()
    {
        String deviceType = "fakedev";
        String deviceId = "12345678";
        String payload = "{\"timestamp\":1477500032627,\"payload\":\"ad574d314e49550020060102010213057845611fbbb304c0a8006530\"}";

        KafkaMessage km = new KafkaMessage(deviceType, deviceId, payload);
        logger.info(km.getJsonHeader());
        logger.info(km.getJsonPayload());

        ObjectMapper mapper = new ObjectMapper();
        try
        {
            KafkaHeader header = mapper.readValue(km.getJsonHeader(), KafkaHeader.class);
            Assert.assertEquals("fakedev", header.getDeviceType());
            Assert.assertEquals("12345678", header.getDeviceId());
            Assert.assertEquals(24, header.getTimestamp().length());
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
        }

    }
}
