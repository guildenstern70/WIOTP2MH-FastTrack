/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.kafka;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "orgId",
        "deviceType",
        "deviceId",
        "eventType",
        "format",
        "timestamp"
})
public class KafkaHeader extends JsonRepresentable implements Serializable
{

    @JsonProperty("orgId")
    private String orgId;
    @JsonProperty("deviceType")
    private String deviceType;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("eventType")
    private String eventType;
    @JsonProperty("format")
    private String format;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();
    private final static long serialVersionUID = 3184407386381055813L;

    @JsonProperty("orgId")
    public String getOrgId()
    {
        return orgId;
    }

    @JsonProperty("orgId")
    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    @JsonProperty("deviceType")
    public String getDeviceType()
    {
        return deviceType;
    }

    @JsonProperty("deviceType")
    public void setDeviceType(String deviceType)
    {
        this.deviceType = deviceType;
    }

    @JsonProperty("deviceId")
    public String getDeviceId()
    {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId)
    {
        this.deviceId = deviceId;
    }

    @JsonProperty("eventType")
    public String getEventType()
    {
        return eventType;
    }

    @JsonProperty("eventType")
    public void setEventType(String eventType)
    {
        this.eventType = eventType;
    }

    @JsonProperty("format")
    public String getFormat()
    {
        return format;
    }

    @JsonProperty("format")
    public void setFormat(String format)
    {
        this.format = format;
    }

    @JsonProperty("timestamp")
    public String getTimestamp()
    {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(String timestamp)
    {
        this.timestamp = timestamp;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(orgId).append(deviceType).append(deviceId).append(eventType).append(format).append(timestamp).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == this)
        {
            return true;
        }
        if (!(other instanceof KafkaHeader))
        {
            return false;
        }
        KafkaHeader rhs = ((KafkaHeader) other);
        return new EqualsBuilder().append(orgId, rhs.orgId).append(deviceType, rhs.deviceType).append(deviceId, rhs.deviceId).append(eventType, rhs.eventType).append(format, rhs.format).append(timestamp, rhs.timestamp).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}


