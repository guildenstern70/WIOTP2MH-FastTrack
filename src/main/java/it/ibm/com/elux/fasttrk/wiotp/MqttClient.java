/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.wiotp;

import com.ibm.iotf.client.app.ApplicationClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class MqttClient
{
    private static final Logger logger = LoggerFactory.getLogger(MqttClient.class);

    private final Properties options;

    public MqttClient(Properties properties)
    {
        this.options = properties;
    }

    public ApplicationClient getIoTClient()
    {
        ApplicationClient client = null;

        try
        {
            client = new ApplicationClient(options);
        }
        catch (Exception exc)
        {
            logger.error(exc.getMessage(), exc);
        }

        return client;
    }
}

