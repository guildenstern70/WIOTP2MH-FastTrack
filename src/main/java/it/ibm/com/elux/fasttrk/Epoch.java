/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 */
public class Epoch
{
    public static String isoTimestamp()
    {
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        df.setTimeZone(tz);
        return df.format(new Date());
    }
}
