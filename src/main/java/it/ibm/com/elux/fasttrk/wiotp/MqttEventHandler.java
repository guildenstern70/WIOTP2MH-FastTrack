/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.wiotp;

import com.ibm.iotf.client.app.Event;
import it.ibm.com.elux.fasttrk.api.EventSubscriber;
import it.ibm.com.elux.fasttrk.api.IEventPublisher;
import it.ibm.com.elux.fasttrk.kafka.KafkaMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Event handler for events received from WIOTP
 */
public class MqttEventHandler extends EventSubscriber implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(MqttEventHandler.class);
    private final Event mqttEvent;

    public MqttEventHandler(IEventPublisher publisher, Event event)
    {
        this.setPublisher(publisher);
        this.mqttEvent = event;
    }

    @Override
    public void run()
    {
        Event e = this.mqttEvent;
        String payload = new String(e.getRawPayload());
        logger.info("Event:: d:" + e.getDeviceType() + ":" + e.getDeviceId() + " >>> " + payload);
        KafkaMessage km = new KafkaMessage(e.getDeviceType(), e.getDeviceId(), payload);
        this.handleEvent(km.getJsonHeader(), km.getJsonPayload());
    }
}
