/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

/**
 *
 */
public abstract class JsonRepresentable
{
    private static final Logger logger = Logger.getLogger(JsonRepresentable.class);

    public String toJSON()
    {
        String json = null;
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            json = mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            logger.error(e.getMessage(), e);
        }
        return json;
    }
}
