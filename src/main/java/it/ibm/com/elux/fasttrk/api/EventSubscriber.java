/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.api;

public abstract class EventSubscriber
{

    // the object that will handle the republishing of
    // events that are received by this object
    private IEventPublisher publisher;

    /**
     * Configures the object that will handle the
     * republishing of events received by this object
     *
     * @param publisher Publisher code handle
     */
    public void setPublisher(IEventPublisher publisher)
    {
        this.publisher = publisher;
    }

    /**
     * hands off the event to the configured publisher (if
     * there is one).  This method should be called by concrete
     * implementations of this class to republish events.
     *
     * @param jsonEventStr Event in JSON format
     */
    public void handleEvent(String eventKey, String jsonEventStr)
    {
        if (this.publisher != null)
        {
            this.publisher.publishEvent(eventKey, jsonEventStr);
        }
    }
}
