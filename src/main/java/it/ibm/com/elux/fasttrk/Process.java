/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk;

import it.ibm.com.elux.fasttrk.kafka.KafkaPublisher;
import it.ibm.com.elux.fasttrk.kafka.KafkaUtils;
import it.ibm.com.elux.fasttrk.wiotp.MqttClient;
import it.ibm.com.elux.fasttrk.wiotp.MqttReceiver;
import it.ibm.com.elux.fasttrk.wiotp.MqttSubscriber;

import java.util.Properties;

/**
 * FastTrack Process, receiving from WIOTP and forwarding to Kafka
 */
public class Process implements Runnable
{

    private final KafkaPublisher publisher;
    private final MqttReceiver receiver;

    public Process()
    {
        // Initialize Kafka Connection
        Properties kafkaProps = Utils.getProperties("producer.properties");
        KafkaUtils.updateJaasConfiguration(kafkaProps.getProperty("api_key"));
        this.publisher = new KafkaPublisher(kafkaProps, kafkaProps.getProperty("kafka.topic"));

        // Initialize MQTT Broker Connection
        MqttClient mqttClient = new MqttClient(Utils.getProperties("watson.properties"));
        MqttSubscriber subscriber = new MqttSubscriber(this.publisher);
        this.receiver = new MqttReceiver(subscriber, mqttClient);
    }

    @Override
    public void run()
    {
        this.receiver.service();
    }

    public void shutdown()
    {
        if (receiver != null)
            receiver.shutdown();
        if (publisher != null)
            publisher.shutdown();
    }
}
