/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.kafka;

import it.ibm.com.elux.fasttrk.Utils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 *
 */
public class KafkaUtils
{
    private static final Logger logger = Logger.getLogger(KafkaUtils.class);

    private static final String JAAS_CONFIG_PROPERTY = "java.security.auth.login.config";
    private static final String JAAS_TEMPLATE = "jaas.conf.template";

    /**
     * Updates JAAS config file with api key.
     */
    public static void updateJaasConfiguration(String apiKey)
    {
        KafkaUtils.updateJaasConfiguration(apiKey.substring(0, 16), apiKey.substring(16));
    }

    /**
     * Updates JAAS config file with provided credentials.
     */
    public static void updateJaasConfiguration(String username, String password)
    {
        String jaasConfPath = System.getProperty("java.io.tmpdir") + File.separator + "jaas.conf";
        System.setProperty(JAAS_CONFIG_PROPERTY, jaasConfPath);

        OutputStream jaasOutStream = null;

        logger.log(Level.INFO, "Updating JAAS configuration");

        try
        {
            String templateContents = Utils.getContents(JAAS_TEMPLATE);
            jaasOutStream = new FileOutputStream(jaasConfPath, false);

            String fileContents = templateContents
                    .replace("$USERNAME", username)
                    .replace("$PASSWORD", password);

            jaasOutStream.write(fileContents.getBytes(Charset.forName("UTF-8")));
        }
        catch (final IOException e)
        {
            logger.log(Level.ERROR, "Failed accessing to JAAS config file", e);
        }
        finally
        {
            if (jaasOutStream != null)
            {
                try
                {
                    jaasOutStream.close();
                }
                catch (final Exception e)
                {
                    logger.log(Level.WARN, "Error closing generated JAAS config file", e);
                }
            }
        }
    }
}
