/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.kafka;

import it.ibm.com.elux.fasttrk.Epoch;

/**
 *
 */
public class KafkaMessage
{
    private final KafkaHeader header;
    private final KafkaPayload payload;

    public KafkaMessage(String deviceType, String deviceId, String payloadJson)
    {

        this.header = new KafkaHeader();
        this.header.setDeviceType(deviceType);
        this.header.setDeviceId(deviceId);
        this.header.setFormat("json");
        this.header.setTimestamp(Epoch.isoTimestamp());
        this.header.setEventType("event");

        this.payload = KafkaPayload.fromJson(payloadJson);

    }

    public String getJsonHeader()
    {
        return this.header.toJSON();
    }

    public String getJsonPayload()
    {
        return this.payload.toJSON();
    }
}
