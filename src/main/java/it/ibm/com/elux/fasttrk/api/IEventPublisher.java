/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.api;

public interface IEventPublisher
{
    /**
     * Publish the event to some other service
     *
     * @param eventJsonStr - the event json encoded as a String
     */
    void publishEvent(String eventKey, String eventJsonStr);
}
