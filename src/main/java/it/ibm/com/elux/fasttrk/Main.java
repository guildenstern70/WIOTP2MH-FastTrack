/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 *
 */
public class Main
{
    private static final String APP_NAME = "FastTrack Connector";
    private static final String APP_VERSION = "0.4.0";

    private static final Logger logger = Logger.getLogger(Main.class);


    static
    {
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                logger.log(Level.WARN, "Shutdown received.")));

        Thread.setDefaultUncaughtExceptionHandler((t, e) ->
                logger.log(Level.ERROR, "Uncaught Exception on " + t.getName() + " : " + e, e));
    }

    public static void main(String[] args)
    {
        hello();
        new Process().run();
    }

    private static void hello()
    {
        logger.info("************************************************");
        logger.info("   Watson IOT IoT " + APP_NAME + " v." + APP_VERSION);
        logger.info("************************************************");
    }

}
