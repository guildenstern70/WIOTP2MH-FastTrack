/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.kafka;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "timestamp",
        "payload"
})
public class KafkaPayload extends JsonRepresentable implements Serializable
{
    private static final Logger logger = Logger.getLogger(KafkaPayload.class);

    @JsonProperty("timestamp")
    private Long timestamp;
    @JsonProperty("payload")
    private String payload;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();
    private final static long serialVersionUID = -3206195033907316574L;

    @JsonProperty("timestamp")
    public Long getTimestamp()
    {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Long timestamp)
    {
        this.timestamp = timestamp;
    }

    @JsonProperty("payload")
    public String getPayload()
    {
        return payload;
    }

    @JsonProperty("payload")
    public void setPayload(String payload)
    {
        this.payload = payload;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties()
    {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value)
    {
        this.additionalProperties.put(name, value);
    }

    @Override
    public int hashCode()
    {
        return new HashCodeBuilder().append(timestamp).append(payload).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other)
    {
        if (other == this)
        {
            return true;
        }
        if (!(other instanceof KafkaPayload))
        {
            return false;
        }
        KafkaPayload rhs = ((KafkaPayload) other);
        return new EqualsBuilder().append(timestamp, rhs.timestamp).append(payload, rhs.payload).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

    public static KafkaPayload fromJson(String payloadJson)
    {
        KafkaPayload payload = null;
        ObjectMapper mapper = new ObjectMapper();
        try
        {
            payload = mapper.readValue(payloadJson, KafkaPayload.class);
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
        }
        return payload;
    }

}

