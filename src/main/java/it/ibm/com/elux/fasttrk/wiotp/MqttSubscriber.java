/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.wiotp;

import com.ibm.iotf.client.app.Command;
import com.ibm.iotf.client.app.Event;
import com.ibm.iotf.client.app.EventCallback;
import it.ibm.com.elux.fasttrk.Utils;
import it.ibm.com.elux.fasttrk.api.IEventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * The subscriber holds a queue of events.
 * The running worker instantiates a number of threads to take events
 * out of the queue and push them to Kafka.
 */
public class MqttSubscriber implements EventCallback, Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(MqttSubscriber.class);
    private static final ExecutorService executor = Executors.newFixedThreadPool(getNumberOfThreads());

    private final BlockingQueue<Event> evtQueue = new LinkedBlockingQueue<>();
    private final IEventPublisher publisher;

    public MqttSubscriber(IEventPublisher publisher)
    {
        this.publisher = publisher;
    }

    @Override
    public void processEvent(Event e)
    {
        try
        {
            evtQueue.put(e);
        }
        catch (InterruptedException e1)
        {
            logger.error(e1.getMessage(), e1);
        }
    }

    @Override
    public void processCommand(Command cmd)
    {
        logger.info("Command received:: " + cmd);
    }

    @Override
    public void run()
    {
        while (true)
        {
            try
            {
                executor.execute(new MqttEventHandler(this.publisher, evtQueue.take()));
            }
            catch (InterruptedException e)
            {
                logger.warn(e.getMessage());
                executor.shutdown();
            }
        }
    }

    private static int getNumberOfThreads()
    {
        Properties properties = Utils.getProperties("application.properties");
        int threads = 4;
        try
        {
            String strThreads = properties.getProperty("fasttrack.threads");
            threads = Integer.parseInt(strThreads);
        }
        catch (NumberFormatException nfe)
        {
            logger.warn("Cannot read number of threads from application.properties: defaulting to 4");
        }
        logger.info("Setting thread pool to " + String.valueOf(threads) + " parallel threads.");
        return threads;
    }

}
