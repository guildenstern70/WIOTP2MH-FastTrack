/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk.wiotp;

import com.ibm.iotf.client.app.ApplicationClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MqttReceiver
{
    private static final Logger logger = LoggerFactory.getLogger(MqttReceiver.class);

    private final MqttSubscriber evtBack;
    private final ApplicationClient iotClient;

    public MqttReceiver(MqttSubscriber evtBack,
                        MqttClient iot)
    {
        this.evtBack = evtBack;
        this.iotClient = iot.getIoTClient();
    }

    public void shutdown()
    {
        logger.info("Disconnecting from WIOTP...");
        this.iotClient.disconnect();
    }

    public void service()
    {
        try
        {
            logger.info("Initializing service");
            logger.info("Connecting with IoT Client > " + iotClient.toString());
            this.iotClient.connect();

            new Thread(this.evtBack).start();
            this.iotClient.setEventCallback(this.evtBack);
            this.iotClient.subscribeToDeviceEvents();

            logger.info("Listening to all device events...");
        }
        catch (Exception ex)
        {
            logger.error(ex.getMessage(), ex);
        }

    }


}

