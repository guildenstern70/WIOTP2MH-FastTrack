/*
 * Copyright (c) IBM Corporation 2017.
 * This is copyrighted software. All rights reserved.
 * IBM - Watson IOT - Watson IOT Connectivity Project
 */

package it.ibm.com.elux.fasttrk;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 */
public class Utils
{
    private static final Logger logger = Logger.getLogger(Utils.class);

    public static String getContents(String fileName)
    {
        String contents = null;

        try (InputStream inputStream = Utils.class.getClassLoader().getResourceAsStream(fileName))
        {
            if (inputStream != null)
            {
                contents = new BufferedReader(new InputStreamReader(inputStream))
                        .lines().collect(Collectors.joining("\n"));
            }
            else
            {
                throw new FileNotFoundException("Property file '" + fileName + "' not found in the classpath");
            }

        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }

        return contents;

    }

    public static Properties getProperties(String propertiesFileName)
    {
        Properties props = new Properties();

        try (InputStream inputStream = Utils.class.getClassLoader().getResourceAsStream(propertiesFileName))
        {
            if (inputStream != null)
            {
                props.load(inputStream);
            }
            else
            {
                throw new FileNotFoundException("Property file '" + propertiesFileName + "' not found in the classpath");
            }

        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }

        return props;
    }
}
