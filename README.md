# FAST TRACK

Custom Watson IOTP to Kafka Connector

#### DEPLOY TO BLUEMIX

Remember to overwrite `watson-properties` properties file with the one for the wanted environment (ie: UAT -> watson-uat).

    cf api https://api.eu-gb.bluemix.net
    cf login --sso -o [your_org] -s Dev
    cf push

#### DISABLE DIEGO HEALTH CHECK

Remember to disable the Diego health check with:

    cf set-health-check FastTrack none
    
#### SEE LOGS
    
    cf logs FastTrack

#### Changelog
1.1 Added ApplicationClient to use a shared subscription
